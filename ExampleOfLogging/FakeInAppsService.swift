//
//  FakeInAppsService .swift
//  ExampleOfLogging
//
//  Created by Alex Parkhimovich on 6/11/18.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation
class FakeInAppsService {
    func makePurchase(completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            completion()
        }
    }
}
