//
//  ViewController.swift
//  ExampleOfLogging
//
//  Created by Alex Parkhimovich on 6/11/18.
//  Copyright © 2018 Gismart. All rights reserved.
//

import UIKit
import GismartLogger

class ViewController: UIViewController {

    let inappsLogger = InappsLogger(GLLogger.default())
    let inappsManager = FakeInAppsService()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func purchaseAction(_ sender: Any) {
        inappsLogger?.purchaseTap(withPurchase: "ID_PRODUCT", price: 7.99, additionalParameters: ["parametr" : "any"])
        inappsManager.makePurchase { [weak self] in
            // replace fake data for real
            self?.inappsLogger?.purchased(withPrice: 7.99, productIdentifier: "ID_PRODUCT", priceLocale: Locale.current, isLifetimeProduct: false, hasTrial: true, transactionId: "TRASACTION_ID", source: "ViewController")
        }
    }
}

