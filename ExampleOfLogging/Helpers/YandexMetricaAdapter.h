//
//  YandexMetricaAdapter.h
//  DrumPads
//
//  Created by Ales Nemianionak on 11/14/16.
//  Copyright © 2016 user. All rights reserved.
//

#import <GismartLogger/ILogger.h>
#import <GismartLogger/ITimeEventContainer.h>

@interface YandexMetricaAdapter : NSObject <ILogger>

- (instancetype)initWithTimeEventContainer:(id<ITimeEventContainer>)container;

@end
