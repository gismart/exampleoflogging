//
//  FBAnalyticsAdapter.h
//  Real Guitar
//
//  Created by Alex Parkhimovich on 8/30/16.
//  Copyright © 2016 Alex Parhimovich. All rights reserved.
//

#import <GismartLogger/ILogger.h>
#import <GismartLogger/ITimeEventContainer.h>

@interface FBAnalyticsAdapter : NSObject <ILogger>

- (instancetype)initWithTimeEventContainer:(id<ITimeEventContainer>)container;

@end
