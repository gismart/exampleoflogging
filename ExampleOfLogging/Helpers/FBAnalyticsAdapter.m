//
//  FBAnalyticsAdapter.m
//  Real Guitar
//
//  Created by Alex Parkhimovich on 8/30/16.
//  Copyright © 2016 Alex Parhimovich. All rights reserved.
//

#import "FBAnalyticsAdapter.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface FBAnalyticsAdapter()

@property(nonatomic) id<ITimeEventContainer> container;

@end

@implementation FBAnalyticsAdapter

- (instancetype)initWithTimeEventContainer:(id<ITimeEventContainer>)container {
    self = [super init];
    if (self) {
        _container = container;
    }
    return self;
}

- (void)logEvent:(NSString *)eventName {
    [FBSDKAppEvents logEvent:eventName];
}

- (void)logEvent:(NSString *)eventName timed:(BOOL)timed {
    [self logEvent:eventName withParameters:nil timed:timed];
}

- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters {
    [FBSDKAppEvents logEvent:eventName parameters:parameters];
}

- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters timed:(BOOL)timed {
    [FBSDKAppEvents logEvent:eventName parameters:parameters];
    if (timed) {
        [self.container storeStartTimeForEventName:eventName];
    }
}

- (void)endTimedEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters {
    [self.container getUpdatedParametersForEventName:eventName
                                     withParameters:parameters
                                       withNewEvent:^(NSString *updatedName, NSDictionary *updatedParameters, NSError *error) {
        if (!error) {
            [FBSDKAppEvents logEvent:updatedName parameters:updatedParameters];
        }
    }];
}
@end
