//
//  AppsFlyerProxy.h
//  Pianino
//
//  Created by dburchik on 8/29/17.
//  Copyright © 2017 Gismart. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GismartLogger/ILogger.h>

@interface AppsFlyerProxy : NSObject<ILogger>

@end
