//
//  AppsFlyerAdapter.m
//  Pianino
//
//  Created by Alexandra on 8/1/17.
//  Copyright © 2017 Gismart. All rights reserved.
//

#import "AppsFlyerAdapter.h"
#import <AppsFlyerLib/AppsFlyerTracker.h>

@implementation AppsFlyerAdapter
    
- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters {
    [[AppsFlyerTracker sharedTracker] trackEvent:eventName withValues:parameters];
}

- (void)logEvent:(NSString *)eventName {
    [self logEvent:eventName withParameters:nil];
}
    
- (void)logEvent:(NSString *)eventName timed:(BOOL)timed {
    [self logEvent:eventName withParameters:nil];
}
    
- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters timed:(BOOL)timed {
    [self logEvent:eventName withParameters:parameters];
}
    
- (void)endTimedEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters {
}

@end
