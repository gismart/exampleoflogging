//
//  AppsFlyerProxy.m
//  Pianino
//
//  Created by dburchik on 8/29/17.
//  Copyright © 2017 Gismart. All rights reserved.
//

#import "AppsFlyerProxy.h"
#import "AppsFlyerAdapter.h"
#import <AppsFlyerLib/AppsFlyerTracker.h>
@interface AppsFlyerProxy()
@property (nonatomic, readwrite) AppsFlyerAdapter * adapter;
@end

@implementation AppsFlyerProxy

- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters {
    if ([eventName isEqualToString:@"purchase_completed"]) {
        BOOL lifetime = [parameters[@"lifetime"] boolValue];
        if (lifetime) {
            [self.adapter logEvent:@"g_lifetime" withParameters: nil];
        } else {
            BOOL trial = [parameters[@"trial"] boolValue];
            if (trial) {
                [self.adapter logEvent:@"g_sub_trial" withParameters: nil];
            } else {
                [self.adapter logEvent:@"g_sub_wo_trial" withParameters: nil];
            }
        }
    } else if ([eventName isEqualToString:@"ad_performed"]) {
        [self.adapter logEvent:eventName];
    }
}

- (void)logEvent:(NSString *)eventName {
    [self logEvent:eventName withParameters:nil];
}

- (void)logEvent:(NSString *)eventName timed:(BOOL)timed {
    [self logEvent:eventName withParameters:nil];
}

- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters timed:(BOOL)timed {
    [self logEvent:eventName withParameters:parameters];
}

- (void)endTimedEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters {
}


- (AppsFlyerAdapter *)adapter {
    if (_adapter == nil) {
        _adapter = [AppsFlyerAdapter new];
    }
    return _adapter;
}
@end
