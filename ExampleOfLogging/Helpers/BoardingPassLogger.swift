//
//  BoardingPassLogger.swift
//  RealTuner
//
//  Created by dburchik on 7/19/18.
//  Copyright © 2018 GiSmart. All rights reserved.
//

import Foundation
import GismartLogger

@objc public enum BoardingPassReason: Int {
    case free
    case trial
    case restore
}

public struct BoardingPassLogger {
    private static let udKey = "onboarding_pass_logged"
    
    let logger: ILogger

    public init(logger: ILogger) {
        self.logger = logger
    }
    
    public func logBoardingPass(_ reason: BoardingPassReason) {
        let logged = UserDefaults.standard.bool(forKey: BoardingPassLogger.udKey)
        
        if !logged {
            switch reason {
            case .free:
                logger.logEvent("boarding_pass_free")
            case .restore:
                logger.logEvent("boarding_pass_restore")
            default:
                break
            }

            logger.logEvent("boarding_pass")
            
            UserDefaults.standard.set(true, forKey: BoardingPassLogger.udKey)
        }
    }

    public func reset() {
        UserDefaults.standard.set(false, forKey: BoardingPassLogger.udKey)
    }
}
