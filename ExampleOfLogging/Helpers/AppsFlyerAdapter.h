//
//  AppsFlyerAdapter.h
//  Pianino
//
//  Created by Alexandra on 8/1/17.
//  Copyright © 2017 Gismart. All rights reserved.
//

#import <GismartLogger/ILogger.h>

@interface AppsFlyerAdapter : NSObject<ILogger>

@end
