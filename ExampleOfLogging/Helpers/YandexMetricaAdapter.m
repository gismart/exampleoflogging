//
//  YandexMetricaAdapter.m
//  DrumPads
//
//  Created by Ales Nemianionak on 11/14/16.
//  Copyright © 2016 user. All rights reserved.
//

#import "YandexMetricaAdapter.h"
#import <YandexMobileMetrica/YandexMobileMetrica.h>

@interface YandexMetricaAdapter()

@property(nonatomic) id<ITimeEventContainer> container;

@end

@implementation YandexMetricaAdapter

- (instancetype)initWithTimeEventContainer:(id<ITimeEventContainer>)container {
    self = [super init];
    if (self) {
        _container = container;
    }
    return self;
}

- (void)logEvent:(NSString *)eventName {
    [YMMYandexMetrica reportEvent:eventName
                        onFailure:^(NSError * _Nullable error) {
                            NSLog(@"Yandex Metrica error: %@", error.localizedDescription);
                        }];
}

- (void)logEvent:(NSString *)eventName timed:(BOOL)timed {
    [self logEvent:eventName withParameters:nil timed:timed];
}

- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters {
    [YMMYandexMetrica reportEvent:eventName
                       parameters:parameters
                        onFailure:^(NSError * _Nullable error) {
                            NSLog(@"Yandex Metrica error: %@", error.localizedDescription);
                        }];
}

- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters timed:(BOOL)timed {
    [YMMYandexMetrica reportEvent:eventName
                       parameters:parameters
                        onFailure:^(NSError * _Nullable error) {
                            NSLog(@"Yandex Metrica error: %@", error.localizedDescription);
                        }];
    if (timed) {
        [self.container storeStartTimeForEventName:eventName];
    }
}

- (void)endTimedEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters {
    [self.container getUpdatedParametersForEventName:eventName
                                     withParameters:parameters
                                       withNewEvent:^(NSString *updatedName, NSDictionary *updatedParameters, NSError *error) {
        if (!error) {
            [YMMYandexMetrica reportEvent:updatedName
                               parameters:updatedParameters
                                onFailure:^(NSError * _Nullable error) {
                                    NSLog(@"Yandex Metrica error: %@", error.localizedDescription);
                                }];
        }
    }];
}

@end
