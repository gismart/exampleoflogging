//
//  OnboardingViewController.swift
//  ExampleOfLogging
//
//  Created by Alex Parhimovich on 11/6/19.
//  Copyright © 2019 Gismart. All rights reserved.
//

import UIKit
import GismartLogger

class OnboardingViewController: UIViewController {
    let inappsLogger = InappsLogger(GLLogger.default())
    let inappsManager = FakeInAppsService()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func purchaseAction(_ sender: Any) {
        inappsLogger?.purchaseTap(withPurchase: "ID_PRODUCT", price: 7.99, additionalParameters: ["parametr" : "any"])
        inappsManager.makePurchase { [weak self] in
            // replace fake data for real
            self?.inappsLogger?.purchased(withPrice: 7.99, productIdentifier: "ID_PRODUCT", priceLocale: Locale.current, isLifetimeProduct: false, hasTrial: true, transactionId: "TRASACTION_ID", source: "Onboarding")
            BoardingPassLogger(logger: GLLogger.default()).logBoardingPass(.trial)
            self?.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func skipAction(_ sender: Any) {
        BoardingPassLogger(logger: GLLogger.default()).logBoardingPass(.free)
        self.dismiss(animated: false, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
