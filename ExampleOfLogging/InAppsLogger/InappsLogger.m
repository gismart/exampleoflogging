//
//  InappsLogger.m
//  Drums
//
//  Created by Alex Parkhimovich on 8/25/17.
//  Copyright © 2017 Gismart. All rights reserved.
//

#import "InappsLogger.h"

@interface InappsLogger()
@property (nonatomic) id <ILogger> logger;
@end

@implementation InappsLogger
- (instancetype)init:(id <ILogger>)logger {
    self = [super init];
    self.logger = logger;
    return self;
}

- (void)purchaseTapWithPurchase:(NSString *)purchase
                          price:(CGFloat)price
           additionalParameters:(NSDictionary *)additionalParams {
    NSMutableDictionary *params = [@{@"purchase_name" : purchase,
                                     @"price" : @(price),
                                     @"complete_status" : @(NO)} mutableCopy];
    if (additionalParams) {
        [params addEntriesFromDictionary:additionalParams];
    }
    [self.logger logEvent:@"purchase_tap" withParameters:params timed:YES];
}

- (void)purchasedWithPrice:(NSDecimalNumber *)price productIdentifier:(NSString *)productIdentifier priceLocale:(NSLocale *)priceLocale isLifetimeProduct:(BOOL)isLifetime hasTrial:(BOOL)hasTrial transactionId:(NSString *)transactionId source:(NSString *)source {
    [self.logger endTimedEvent:@"purchase_tap" withParameters:@{@"complete_status" : @(YES)}];
    [self.logger logEvent:@"purchase_tap_end" withParameters:@{@"complete_status" : @(YES)}];
    
    NSMutableDictionary *loggerParams = [[self buildParamsWithPrice:price productIdentifier:productIdentifier priceLocale:priceLocale isLifetimeProduct:isLifetime hasTrial:hasTrial transactionId: transactionId source: source] mutableCopy];
    
    [self.logger logEvent:@"purchase_completed" withParameters:loggerParams];
}

- (NSDictionary *)buildParamsWithPrice:(NSDecimalNumber *)price productIdentifier:(NSString *)productIdentifier priceLocale:(NSLocale *)priceLocale isLifetimeProduct:(BOOL)isLifetime hasTrial:(BOOL)hasTrial transactionId:(NSString *)transactionId source:(NSString *)source {
    NSLocale *locale = priceLocale;
    NSString *code = [locale objectForKey:NSLocaleCurrencyCode];
    return @{
             @"current_product_id" : productIdentifier,
             @"price" : price,
             @"currency" : code,
             @"lifetime" : @(isLifetime),
             @"trial" : @(hasTrial),
             @"original_transaction_id": transactionId,
             @"source" : source
             };
}

@end
