//
//  InappsLogger.h
//  Drums
//
//  Created by Alex Parkhimovich on 8/25/17.
//  Copyright © 2017 Gismart. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <GismartLogger/ILogger.h>
@interface InappsLogger : NSObject

- (instancetype)init:(id <ILogger>)logger;

- (void)purchaseTapWithPurchase:(NSString *)purchase
                          price:(CGFloat)price
           additionalParameters:(NSDictionary *)additionalParams;

- (void)purchasedWithPrice:(NSDecimalNumber *)price
         productIdentifier:(NSString *)productIdentifier
               priceLocale:(NSLocale *)priceLocale
         isLifetimeProduct:(BOOL)isLifetime
                  hasTrial:(BOOL)hasTrial
             transactionId:(NSString *)transactionId
                    source:(NSString *)source;
@end
